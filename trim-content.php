<?php

$s = 'oil,mineral,paraffin,multivitamins,         
zinc oxide    , 
color 3,test,                                                magnesium oxide,   protein ,
clindamycin,
Ziana Gel,

water,
water,
 salicylic acid ,
benzoyl peroxide,
air,
vitamin,
aqua';

// Variable includeing space and tag.
$s = "oil,mineral,paraffin,multivitamins,         <br />
zinc oxide    , <br />
color 3,test,                                                magnesium oxide,   protein ,<br />
clindamycin,<br />
Ziana Gel,<br />
<br />
water,<br />
water,<br />
 salicylic acid ,<br />
benzoyl peroxide,<br />
air,<br />
vitamin,<br />
aqua";

function trim_value(&$value) 
{ 
    $value = trim($value,' '); 
}
/**
* Function to trim all type of whitespace, null characters, new line etc.
*/
function trim_all( $str , $what = NULL , $with = ' ' )
{
    if( $what === NULL )
    {
        //  Character      Decimal      Use
        //  "\0"            0           Null Character
        //  "\t"            9           Tab
        //  "\n"           10           New line
        //  "\x0B"         11           Vertical Tab
        //  "\r"           13           New Line in Mac
        //  " "            32           Space
       
        $what   = "\\x00-\\x20";    //all white-spaces and control chars
    }
   
    return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
}
$variations = preg_replace('/^(<br\s*\/?>)*|(<br\s*\/?>)*$/i', '', $s);

$t = explode(',', $variations);
$y = [];
foreach($t as $z){
    //$y[] = trim($z, ' ');   
    // Remove all special characters
    
    $str = preg_replace('/[^A-Za-z0-9]/', ' ', strip_tags($z));
    $y[] = trim(trim_all(strip_tags($str)));
}
echo '<pre>';
print_r($y);

$variations_str = implode(',', $y);
var_dump($variations_str);
